from flask_mysqldb import MySQL
import yaml
import json

class Database:
    db = 0
    mysql = 0
    roll = 1

    def __init__(self,app):
        self.db = yaml.load(open('db.yaml'))
        app.config['MYSQL_HOST'] = self.db['mysql_host']
        app.config['MYSQL_USER'] = self.db['mysql_user']
        app.config['MYSQL_PASSWORD'] = self.db['mysql_password']
        app.config['MYSQL_DB'] = self.db['mysql_db']
        self.mysql = MySQL(app)

    def insert(self,details):
        name = details["name"]
        univ = details["university"]
        cur = self.mysql.connection.cursor()
        self.roll = self.roll + 1
        cur.execute("INSERT INTO students(name,roll,univerity) VALUES(%s, %s, %s)",(name,self.roll , univ))
        self.mysql.connection.commit()
        cur.close()
        return self.roll

    def getDetails(self,detail):
        cur = self.mysql.connection.cursor()
        resultValue = cur.execute("SELECT * FROM students WHERE roll=%s",detail["roll"])
        print(resultValue)
        if resultValue > 0:
            studentsDetails = cur.fetchall()
        items=[]
        for row in studentsDetails:
            items.append({"name":row[0],"roll":row[1],"university":row[2]})
        return json.dumps(items)