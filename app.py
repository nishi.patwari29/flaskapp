import re
from db import Database
from flask import Flask,render_template,request,redirect

app = Flask(__name__)
dbObject = Database(app)

@app.route('/addStudent', methods=['POST'])
def index():
    studentDetails = request.get_json()
    roll = dbObject.insert(studentDetails)
    return "{\"roll\":%s}"%roll

@app.route('/getStudent', methods=['GET'])
def users():
    roll = request.get_json()
    details= dbObject.getDetails(roll)
    return details

if __name__ == '__main__':
    app.run(debug=True)
